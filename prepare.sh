#!/bin/bash

#prepare mbart
ROOT=/data/ecoli/ikcest


declare -A site
site["fr"]="fr_XX"
site["ru"]="ru_RU"
site["th"]="th_TH"

if [ ! -d "mbart.cc25.v2" ];then
wget https://dl.fbaipublicfiles.com/fairseq/models/mbart/mbart.cc25.v2.tar.gz
tar -xf mbart.cc25.v2.tar.gz
fi

EXTRA=$ROOT/extraData
# download extra dataset 
if [ ! -d "extraData" ];then
mkdir extraData
wget https://object.pouta.csc.fi/OPUS-TED2020/v1/moses/th-zh_cn.txt.zip -O extraData/th-zh.zip
unzip extraData/th-zh.zip -d extraData/th-zh

wget https://object.pouta.csc.fi/OPUS-TED2020/v1/moses/fr-zh_cn.txt.zip -O extraData/fr-zh.zip
unzip extraData/fr-zh.zip -d extraData/fr-zh

wget https://object.pouta.csc.fi/OPUS-TED2020/v1/moses/ru-zh_cn.txt.zip -O extraData/ru-zh.zip
unzip extraData/ru-zh.zip -d extraData/ru-zh
fi

PATHTOMBART=$ROOT/mbart.cc25.v2

raw=$ROOT/raw
datasets=$ROOT/datasets
#cut the train date#
mkdir raw
for lang in fr ru th;
do
mkdir $raw/$lang'-zh'

cut -f 1 $datasets/$lang'_zh.train' > $raw/$lang'-zh'/$lang'-zh'.train.$lang
cut -f 2 $datasets/$lang'_zh.train' > $raw/$lang'-zh'/$lang'-zh'.train.zh
cp ./datasets/$lang'_zh.test' raw/$lang'-zh'/

mkdir $raw/'zh-'$lang
cut -f 1 $datasets/'zh_'$lang'.train' > $raw/'zh-'$lang/'zh-'$lang.train.zh
cut -f 2 $datasets/'zh_'$lang'.train' > $raw/'zh-'$lang/'zh-'$lang.train.$lang
cp ./datasets/'zh_'$lang.test raw/'zh-'$lang/
done

# clean (Only discard overly long items)
for lang  in fr ru th;
do
# clean raw
python ./preprocess.py $raw/$lang'-zh'/$lang'-zh'.train.zh $raw/$lang'-zh'/$lang'-zh'.train.$lang $lang
python ./preprocess.py $raw/'zh-'$lang/'zh-'$lang.train.zh $raw/'zh-'$lang/'zh-'$lang.train.$lang $lang

# clean extra 
python ./preprocess.py $EXTRA/$lang"-zh"/TED2020.$lang"-zh_cn".zh_cn  $EXTRA/$lang"-zh"/TED2020.$lang"-zh_cn".$lang $lang
done


TOSPM=$ROOT/tospm.py
SPILIT=$ROOT/train_data_spilit.py
# split and bpe
for lang in fr ru th;
do
cd $raw/'zh-'$lang

#merge corpus 
cat $raw/'zh-'$lang/'zh-'$lang.train.$lang.clean $EXTRA/$lang-zh/TED2020.$lang"-zh_cn".$lang.clean > 'zh-'$lang.train.clean.$lang
cat $raw/'zh-'$lang/'zh-'$lang.train.zh.clean $EXTRA/$lang-zh/TED2020.$lang"-zh_cn".zh_cn.clean > 'zh-'$lang.train.clean.zh


python $SPILIT zh $lang $raw/'zh-'$lang/'zh-'$lang.train.clean \
$raw/'zh-'$lang/data 20000

mkdir bpe 

python $TOSPM $PATHTOMBART/sentence.bpe.model \
< data/train.$lang  > bpe/train.bpe.${site[$lang]}

python $TOSPM $PATHTOMBART/sentence.bpe.model \
< data/dev.$lang  > bpe/valid.bpe.${site[$lang]}

python $TOSPM $PATHTOMBART/sentence.bpe.model \
< data/dev.zh  > bpe/valid.bpe.zh_CN

python $TOSPM $PATHTOMBART/sentence.bpe.model \
< data/train.zh  > bpe/train.bpe.zh_CN


cd $raw/$lang'-zh'

cat $raw/$lang'-zh'/$lang'-zh'.train.$lang.clean $EXTRA/$lang-zh/TED2020.$lang"-zh_cn".$lang.clean > $lang'-zh'.train.clean.$lang
cat $raw/$lang'-zh'/$lang'-zh'.train.zh.clean $EXTRA/$lang-zh/TED2020.$lang"-zh_cn".zh_cn.clean > $lang'-zh'.train.clean.zh

python $SPILIT $lang zh $raw/$lang'-zh'/$lang'-zh'.train.clean \
$raw/$lang'-zh'/data 20000

mkdir bpe 

python $TOSPM $PATHTOMBART/sentence.bpe.model \
< data/train.$lang  > bpe/train.bpe.${site[$lang]}

python $TOSPM $PATHTOMBART/sentence.bpe.model \
< data/dev.$lang  > bpe/valid.bpe.${site[$lang]}

python $TOSPM $PATHTOMBART/sentence.bpe.model \
< data/dev.zh  > bpe/valid.bpe.zh_CN

python $TOSPM $PATHTOMBART/sentence.bpe.model \
< data/train.zh  > bpe/train.bpe.zh_CN

done 


# # split train and dev #
# split=$ROOT/train_data_spilit.py
# for lang in fr ru th;
# do
# python $split $lang zh $raw/$lang'-zh'/$lang'-zh'.train $raw/$lang'-zh'/ 2000
# python $split zh $lang $raw/'zh-'$lang/'zh-'$lang.train $raw/'zh-'$lang/ 2000
# done
